<?php

namespace RESWUE\TrustTest;

use PHPUnit\Framework\TestCase;
use RESWUE\Trust\GroupMemberships;

class GroupsTest extends TestCase
{
    public function testGroups()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../assets/groups.json'), true);

        $groups = new GroupMemberships($data);
        $firstMembership = $groups->getMemberships()[0];
        $this->assertInstanceOf(GroupMemberships\Membership::class, $firstMembership);
        $this->assertEquals('2a1da5a4-1c73-47e9-b430-2cdf524e1f3b', $firstMembership->getGroupId());
        $this->assertEquals('moderator', $firstMembership->getState());


        $secondMembership = $groups->getMemberships()[1];
        $this->assertInstanceOf(GroupMemberships\Membership::class, $secondMembership);
        $this->assertEquals('b68cda64-a38d-4037-a1cc-d7a6d5862fb6', $secondMembership->getGroupId());
        $this->assertEquals('member', $secondMembership->getState());

        $i2a = iterator_to_array($groups);
        $this->assertSame($firstMembership, $i2a[0]);
        $this->assertSame($secondMembership, $i2a[1]);
    }
}
