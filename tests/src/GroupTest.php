<?php

namespace RESWUE\TrustTest;

use PHPUnit\Framework\TestCase;
use RESWUE\Trust\Group;
use RESWUE\Trust\Groups;

class GroupTest extends TestCase
{
    public function testGroupWithJoinLink()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../assets/group_with_joinlink.json'), true);

        $group = new Group($data);
        $this->assertEquals('Trustmaster Fanclub', $group->getName());
        $this->assertEquals('https://trust.reswue.net/groups/2a1da5a4-1c73-47e9-b430-2cdf524e1f3b/join/li5zEyVS3iUNR4g28mkhjnAcBWAF3veB6FF8RCud', $group->getJoinUrl());
    }

    public function testGroup()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../assets/group.json'), true);

        $group = new Group($data);
        $this->assertEquals('Trustmaster Fanclub', $group->getName());
        $this->assertNull($group->getJoinUrl());
    }
}