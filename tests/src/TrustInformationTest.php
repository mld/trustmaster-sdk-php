<?php

namespace RESWUE\TrustTest;

use PHPUnit\Framework\TestCase;
use RESWUE\Trust\ExternalProfile;
use RESWUE\Trust\TrustInformation;

class TrustInformationTest extends TestCase
{
    public function testTrust()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../assets/trust_information.json'), true);

        $trust = new TrustInformation($data);
        $this->assertEquals(0, $trust->getGeneration());
        $this->assertEquals('908afb20-814f-58fa-a9ac-81719fe599f5', $trust->getTrustmasterId());
        $this->assertInstanceOf(TrustInformation\Trust::class, $trust->getNewestTrust());
        $this->assertInstanceOf(TrustInformation\Trust::class, $trust->getTrust());
        $this->assertInstanceOf(TrustInformation\Summary::class, $trust->getSummary());
        $this->assertInstanceOf(TrustInformation\Summary::class, $trust->getSummaryUnverified());

        $this->assertInstanceOf(\DateTime::class, $trust->getNewestTrust()->getUpdatedAt());
        $this->assertEquals('approver', $trust->getNewestTrust()->getDecision());

        $this->assertInstanceOf(\DateTime::class, $trust->getTrust()->getUpdatedAt());
        $this->assertEquals('admin', $trust->getTrust()->getDecision());

        $this->assertEquals(1, $trust->getSummary()->getAdmin());
        $this->assertEquals(2, $trust->getSummary()->getApprover());
        $this->assertEquals(2, $trust->getSummary()->getFully());
        $this->assertEquals(1, $trust->getSummary()->getBasic());
        $this->assertEquals(0, $trust->getSummary()->getFrog());
        $this->assertEquals(0, $trust->getSummary()->getUntrusted());
        $this->assertEquals(0, $trust->getSummary()->getDeactivated());

        $this->assertEquals(0, $trust->getSummaryUnverified()->getAdmin());
        $this->assertEquals(2, $trust->getSummaryUnverified()->getApprover());
        $this->assertEquals(5, $trust->getSummaryUnverified()->getFully());
        $this->assertEquals(2, $trust->getSummaryUnverified()->getBasic());
        $this->assertEquals(1, $trust->getSummaryUnverified()->getFrog());
        $this->assertEquals(3, $trust->getSummaryUnverified()->getUntrusted());
        $this->assertEquals(1, $trust->getSummaryUnverified()->getDeactivated());

    }
    
    public function testTrustUnknown()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../assets/trust_information_unknown.json'), true);

        $trust = new TrustInformation($data);
        $this->assertEquals(-1, $trust->getGeneration());
        $this->assertEquals('908afb20-814f-58fa-a9ac-81719fe599f5', $trust->getTrustmasterId());
        $this->assertNull($trust->getNewestTrust());
        $this->assertNull($trust->getTrust());
        $this->assertInstanceOf(TrustInformation\Summary::class, $trust->getSummary());
        $this->assertInstanceOf(TrustInformation\Summary::class, $trust->getSummaryUnverified());

        $this->assertEquals(0, $trust->getSummary()->getAdmin());
        $this->assertEquals(0, $trust->getSummary()->getApprover());
        $this->assertEquals(0, $trust->getSummary()->getFully());
        $this->assertEquals(0, $trust->getSummary()->getBasic());
        $this->assertEquals(0, $trust->getSummary()->getFrog());
        $this->assertEquals(0, $trust->getSummary()->getUntrusted());
        $this->assertEquals(0, $trust->getSummary()->getDeactivated());

        $this->assertEquals(0, $trust->getSummaryUnverified()->getAdmin());
        $this->assertEquals(0, $trust->getSummaryUnverified()->getApprover());
        $this->assertEquals(0, $trust->getSummaryUnverified()->getFully());
        $this->assertEquals(0, $trust->getSummaryUnverified()->getBasic());
        $this->assertEquals(0, $trust->getSummaryUnverified()->getFrog());
        $this->assertEquals(0, $trust->getSummaryUnverified()->getUntrusted());
        $this->assertEquals(0, $trust->getSummaryUnverified()->getDeactivated());

    }
}
