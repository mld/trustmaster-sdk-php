<?php

namespace RESWUE\Trust;

use GuzzleHttp\Client as HttpClient;

/**
 * \RESWUE\Trust\Client is a PHP client to interact with RESWUE Trustmaster.
 */
class Client
{
    /**
     * The HTTP Client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    protected $clientId;
    protected $secret;
    protected $url;
    protected $userAgent = 'trustmaster-sdk-php';
    protected $redirectUri;
    protected $scopes = [];
    protected $version;

    private $connect_timeout_sec = 2.0;
    private $timeout_sec = 5.0;

    public function __construct($clientId, $secret, $url = null, $version = 4)
    {
        $this->clientId = $clientId;
        $this->secret = $secret;
        $this->url = (empty($url) ? 'https://trust.reswue.net' : $url);
        $this->version = $version;
    }

    /**
     * Returns the Trustmaster URL used.
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Returns the client ID used.
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Gets the connect timeout.
     */
    public function getConnectTimeout(): float
    {
        return $this->connect_timeout_sec;
    }


    /**
     * Sets the connect timeout.
     *
     * @param float $timeout
     */
    public function setConnectTimeout(float $timeout)
    {
        $this->connect_timeout_sec = $timeout;
    }
    
    /**
     * Gets the response timeout.
     */
    public function getTimeout(): float
    {
        return $this->timeout_sec;
    }

    /**
     * Sets the response timeout.
     *
     * @param float $timeout
     */
    public function setTimeout(float $timeout)
    {
        $this->timeout_sec = $timeout;
    }

    /**
     * Fetches a 'client_credentials' OAuth token from Trustmaster.
     *
     * Note: Store this token somewhere safe for later use, in order to
     *       avoid this request.
     *
     * @param array $scopes
     * @return  \RESWUE\Trust\AccessToken
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fetchClientCredentialsGrant(array $scopes = [])
    {
        $response = $this->getHttpClient()->request(
            'POST',
            $this->url.'/oauth/token',
            [
                'timeout' => $this->timeout_sec,
                'connect_timeout' => $this->connect_timeout_sec,
                'form_params' => [
                    'grant_type'    => 'client_credentials',
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secret,
                    'scope'         => implode(' ', $scopes),
                ],
                'headers' => [
                    'Accept'        => 'application/json',
                    'User-Agent'    => $this->userAgent,
                ],
            ]
        );

        return new AccessToken(json_decode((string) $response->getBody(), true));
    }

    /**
     * Fetches Trust information for the user identified by the given $googleId.
     * The request is authenticated using the given AccessToken
     *
     * @param   string                      $googleId
     * @param   \RESWUE\Trust\AccessToken   $token
     * @return  mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTrustInformationByGoogleId($googleId, AccessToken $token)
    {
        if ($this->version >= 4) {
            $googleId = 'google:'.$googleId;
        }

        return $this->getTrustInformation($googleId, $token);
    }

    /**
     * @param string $telegramId
     * @param AccessToken $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTrustInformationByTelegramId($telegramId, AccessToken $token)
    {
        return $this->getTrustInformation('telegram:'.$telegramId, $token);
    }

    /**
     * Fetches Trust information for the user identified by the given $id.
     * The request is authenticated using the given AccessToken
     *
     * @param string $id
     * @param AccessToken $token
     * @param bool $returnObject only works with v4 and higher
     * @return array|TrustInformation
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function getTrustInformation($id, AccessToken $token, bool $returnObject=false)
    {
        $response = $this->getHttpClient()->request(
            'GET',
            $this->url.'/api/v'.$this->version.'/user/'.$id.'/trust',
            [
                'timeout' => $this->timeout_sec,
                'connect_timeout' => $this->connect_timeout_sec,
                'headers' => [
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $token,
                    'User-Agent'    => $this->userAgent,
                ],
            ]
        );

        $data = json_decode((string) $response->getBody(), true);
        if ($returnObject === true && $this->version >= 4) {
            return new TrustInformation($data);
        }
        return $data;
    }

    /**
     * Sets a custom User-Agent.
     *
     * @param  string $userAgent
     * @return $this
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Returns the current User-Agent.
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * Get a instance of the Guzzle HTTP client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function getHttpClient()
    {
        if (is_null($this->httpClient)) {
            $this->httpClient = new HttpClient();
        }

        return $this->httpClient;
    }

    /**
     * Set the Guzzle HTTP client instance.
     *
     * @param  \GuzzleHttp\Client  $client
     * @return $this
     */
    public function setHttpClient(HttpClient $client)
    {
        $this->httpClient = $client;

        return $this;
    }

    /**
     * @param string $uri
     */
    public function setRedirectUri($uri)
    {
        $this->redirectUri = $uri;
    }

    /**
     * @param array $scopes
     */
    public function setScopes(array $scopes)
    {
        $this->scopes = $scopes;
    }

    /**
     * Creates an authurl
     *
     * @return string
     */
    public function createAuthUrl()
    {
        $params = [
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectUri,
            'response_type' => 'code',
            'scope' => implode(' ', $this->scopes),
        ];
        $url = $this->url.'/oauth/authorize';

        return $url.'?'.http_build_query($params, '', '&');
    }

    /**
     * @param string $code
     * @return AccessToken
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function authenticate(string $code)
    {
        $response = $this->getHttpClient()->request(
            'POST',
            $this->url.'/oauth/token',
            [
                'timeout' => $this->timeout_sec,
                'connect_timeout' => $this->connect_timeout_sec,
                'form_params' => [
                    'grant_type'    => 'authorization_code',
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secret,
                    'scope'         => implode(' ', $this->scopes),
                    'code' => $code,
                    'redirect_uri' => $this->redirectUri,
                ],
                'headers' => [
                    'Accept'        => 'application/json',
                    'User-Agent'    => $this->userAgent,
                ],
            ]
        );

        return new AccessToken(json_decode((string) $response->getBody(), true));
    }

    /**
     * @param   string $refreshToken
     * @return  \RESWUE\Trust\AccessToken
     * @throws  \GuzzleHttp\Exception\GuzzleException
     */
    public function refresh(string $refreshToken)
    {
        $response = $this->getHttpClient()->request(
            'POST',
            $this->url.'/oauth/token',
            [
                'timeout' => $this->timeout_sec,
                'connect_timeout' => $this->connect_timeout_sec,
                'form_params' => [
                    'grant_type'    => 'refresh_token',
                    'client_id'     => $this->clientId,
                    'client_secret' => $this->secret,
                    'refresh_token' => $refreshToken
                ],
                'headers' => [
                    'Accept'        => 'application/json',
                    'User-Agent'    => $this->userAgent,
                ],
            ]
        );

        return new AccessToken(json_decode((string) $response->getBody(), true));
    }

    /**
     * @param AccessToken $token
     * @return Profile
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function me(AccessToken $token)
    {
        $me = $this->call($token,'/me');
        switch ($this->version) {
            case 2:
                $profile = new ProfileV2($me);
                break;
            default:
                $profile = new Profile($me);
                break;
        }

        return $profile;
    }

    /**
     * @param string $trustmasterId
     * @param string $url
     * @param AccessToken $token
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setExternalProfile($trustmasterId, $url, AccessToken $token)
    {
        $path = '/user/'.$trustmasterId.'/external_profile';
        $this->call($token, $path, ['external_profile' => $url], 'PUT');

        return true;
    }

    /**
     * @param string $trustmasterId
     * @param AccessToken $token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getExternalProfile($trustmasterId, AccessToken $token)
    {
        $path = '/user/'.$trustmasterId.'/external_profile';
        $externalProfile = $this->call($token, $path);

        return new ExternalProfile($externalProfile);
    }

    /**
     * @param string $trustmasterId
     * @param AccessToken $token
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function deleteExternalProfile($trustmasterId, AccessToken $token)
    {
        $path = '/user/'.$trustmasterId.'/external_profile';
        $this->call($token, $path, null, 'DELETE');

        return true;
    }

    /**
     * @param string $trustmasterId
     * @param AccessToken $token
     * @return Groups
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getGroupMemberships($trustmasterId, AccessToken $token)
    {
        $path = '/user/'.$trustmasterId.'/groups';
        $data = $this->call($token, $path);

        return new GroupMemberships($data);
    }

    /**
     * @param $groupId
     * @param AccessToken $token
     * @return Group
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getGroup($groupId, AccessToken $token)
    {
        $path = '/group/'.$groupId;
        $data = $this->call($token, $path);

        return new Group($data);
    }

    /**
     * @param AccessToken $token
     * @param string $path
     * @param array $params
     * @param string $method
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function call(AccessToken $token, $path, $params = [], $method = 'GET')
    {
        $uri = $this->url . '/api/v'.$this->version . $path;

        $response = $this->getHttpClient()->request(
            $method,
            $uri,
            [
                'timeout' => $this->timeout_sec,
                'connect_timeout' => $this->connect_timeout_sec,
                'headers' => [
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer ' . $token->getToken(),
                    'User-Agent'    => $this->userAgent,
                ],
                'json' => $params,
            ]
        );

        return json_decode((string) $response->getBody(), true);
    }
}
