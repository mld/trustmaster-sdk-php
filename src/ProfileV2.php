<?php

namespace RESWUE\Trust;

class ProfileV2
{
    private $googleId;
    private $agentName;
    private $googleAvatar;
    private $googleName;
    private $email;
    private $telegramUsername;
    private $telegramId;
    private $locationLat;
    private $locationLng;
    private $locationPrivacy;
    private $zelloUsername;

    public function __construct(array $data)
    {
        $this->setGoogleId($data['google_id']);

        if (isset($data['google_avatar'])) {
            $this->setGoogleAvatar($data['google_avatar']);
        }

        if (isset($data['google_name'])) {
            $this->setGoogleName($data['google_name']);
        }

        if (isset($data['email'])) {
            $this->setEmail($data['email']);
        }

        if (isset($data['telegram']['telegram_username'])) {
            $this->setTelegramUsername($data['telegram']['telegram_username']);
        }

        if (isset($data['telegram']['telegram_id'])) {
            $this->setTelegramId($data['telegram']['telegram_id']);
        }

        if (isset($data['location']['lat'])) {
            $this->setLocationLat($data['location']['lat']);
        }

        if (isset($data['location']['lng'])) {
            $this->setLocationLng($data['location']['lng']);
        }

        if (isset($data['location']['privacy'])) {
            $this->setLocationPrivacy($data['location']['privacy']);
        }

        if (isset($data['agent_name'])) {
            $this->setAgentName($data['agent_name']);
        }

        if (isset($data['zello_username'])) {
            $this->setZelloUsername($data['zello_username']);
        }
    }

    /**
     * @return string
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * @param string $googleId
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
    }

    /**
     * @return string
     */
    public function getAgentName()
    {
        return $this->agentName;
    }

    /**
     * @param string $agentName
     */
    public function setAgentName($agentName)
    {
        $this->agentName = $agentName;
    }

    /**
     * @return string
     */
    public function getGoogleAvatar()
    {
        return $this->googleAvatar;
    }

    /**
     * @param string $googleAvatar
     */
    public function setGoogleAvatar($googleAvatar)
    {
        $this->googleAvatar = $googleAvatar;
    }

    /**
     * @return string
     */
    public function getGoogleName()
    {
        return $this->googleName;
    }

    /**
     * @param string $googleName
     */
    public function setGoogleName($googleName)
    {
        $this->googleName = $googleName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTelegramUsername()
    {
        return $this->telegramUsername;
    }

    /**
     * @param string $telegramUsername
     */
    public function setTelegramUsername($telegramUsername)
    {
        $this->telegramUsername = $telegramUsername;
    }

    /**
     * @return string
     */
    public function getTelegramId()
    {
        return $this->telegramId;
    }

    /**
     * @param string $telegramId
     */
    public function setTelegramId($telegramId)
    {
        $this->telegramId = $telegramId;
    }

    /**
     * @return string
     */
    public function getLocationLat()
    {
        return $this->locationLat;
    }

    /**
     * @param string $locationLat
     */
    public function setLocationLat($locationLat)
    {
        $this->locationLat = $locationLat;
    }

    /**
     * @return string
     */
    public function getLocationLng()
    {
        return $this->locationLng;
    }

    /**
     * @param string $locationLng
     */
    public function setLocationLng($locationLng)
    {
        $this->locationLng = $locationLng;
    }

    /**
     * @return string
     */
    public function getLocationPrivacy()
    {
        return $this->locationPrivacy;
    }

    /**
     * @param string $locationPrivacy
     */
    public function setLocationPrivacy($locationPrivacy)
    {
        $this->locationPrivacy = $locationPrivacy;
    }

    /**
     * @return string
     */
    public function getZelloUsername()
    {
        return $this->zelloUsername;
    }

    /**
     * @param string $zelloUsername
     */
    public function setZelloUsername($zelloUsername)
    {
        $this->zelloUsername = $zelloUsername;
    }
}