<?php

namespace RESWUE\Trust;

use RESWUE\Trust\Profile\Location;
use RESWUE\Trust\Profile\Social;

class Profile
{
    private $trustmasterId;
    private $agentName;
    private $email;
    private $name;
    private $avatar;
    private $social;
    private $location;
    private $languages;

    public function __construct(array $data)
    {
        $this->setTrustmasterId($data['trustmaster_id']);

        if (isset($data['agent_name'])) {
            $this->setAgentName($data['agent_name']);
        }

        if (isset($data['email'])) {
            $this->setEmail($data['email']);
        }

        if (isset($data['name'])) {
            $this->setName($data['name']);
        }

        if (isset($data['avatar'])) {
            $this->setAvatar($data['avatar']);
        }

        if (isset($data['social'])) {
            $this->setSocial(new Social($data['social']));
        }

        if (isset($data['location'])) {
            $this->setLocation(new Location($data['location']));
        }

        if (isset($data['languages'])) {
            $this->setLanguages($data['languages']);
        }
    }

    /**
     * @return string
     */
    public function getTrustmasterId()
    {
        return $this->trustmasterId;
    }

    /**
     * @param string $trustmasterId
     */
    public function setTrustmasterId($trustmasterId): void
    {
        $this->trustmasterId = $trustmasterId;
    }

    /**
     * @return string
     */
    public function getAgentName()
    {
        return $this->agentName;
    }

    /**
     * @param string $agentName
     */
    public function setAgentName($agentName): void
    {
        $this->agentName = $agentName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar($avatar): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @return Social
     */
    public function getSocial()
    {
        return $this->social;
    }

    /**
     * @param Social $social
     */
    public function setSocial(Social $social): void
    {
        $this->social = $social;
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Location $location
     */
    public function setLocation(Location $location): void
    {
        $this->location = $location;
    }

    /**
     * @return array
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param array $languages
     */
    public function setLanguages(array $languages): void
    {
        $this->languages = $languages;
    }


}