<?php

namespace RESWUE\Trust\Profile;


class Location
{
    private $lat;
    private $lng;
    private $privacy;

    public function __construct(array $data)
    {
        if (isset($data['lat'])) {
            $this->setLat($data['lat']);
        }

        if (isset($data['lng'])) {
            $this->setLng($data['lng']);
        }

        if (isset($data['privacy'])) {
            $this->setPrivacy($data['privacy']);
        }
    }


    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat($lat): void
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng($lng): void
    {
        $this->lng = $lng;
    }

    /**
     * @return string
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * @param string $privacy
     */
    public function setPrivacy($privacy): void
    {
        $this->privacy = $privacy;
    }


}