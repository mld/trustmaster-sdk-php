<?php

namespace RESWUE\Trust\TrustInformation;


class Summary
{
    private $basic = 0;
    private $fully = 0;
    private $approver = 0;
    private $admin = 0;
    private $frog = 0;
    private $untrusted = 0;
    private $deactivated = 0;

    public function __construct(array $data = [])
    {
        if (isset($data['basic'])) {
            $this->setBasic($data['basic']);
        }
        if (isset($data['fully'])) {
            $this->setFully($data['fully']);
        }
        if (isset($data['approver'])) {
            $this->setApprover($data['approver']);
        }
        if (isset($data['admin'])) {
            $this->setAdmin($data['admin']);
        }
        if (isset($data['frog'])) {
            $this->setFrog($data['frog']);
        }
        if (isset($data['untrusted'])) {
            $this->setUntrusted($data['untrusted']);
        }
        if (isset($data['deactivated'])) {
            $this->setDeactivated($data['deactivated']);
        }
    }

    /**
     * @return integer
     */
    public function getBasic()
    {
        return $this->basic;
    }

    /**
     * @param integer $basic
     */
    public function setBasic($basic): void
    {
        $this->basic = $basic;
    }

    /**
     * @return integer
     */
    public function getFully()
    {
        return $this->fully;
    }

    /**
     * @param integer $fully
     */
    public function setFully($fully): void
    {
        $this->fully = $fully;
    }

    /**
     * @return integer
     */
    public function getApprover()
    {
        return $this->approver;
    }

    /**
     * @param integer $approver
     */
    public function setApprover($approver): void
    {
        $this->approver = $approver;
    }

    /**
     * @return integer
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param integer $admin
     */
    public function setAdmin($admin): void
    {
        $this->admin = $admin;
    }

    /**
     * @return integer
     */
    public function getFrog()
    {
        return $this->frog;
    }

    /**
     * @param integer $frog
     */
    public function setFrog($frog): void
    {
        $this->frog = $frog;
    }

    /**
     * @return integer
     */
    public function getUntrusted()
    {
        return $this->untrusted;
    }

    /**
     * @param integer $untrusted
     */
    public function setUntrusted($untrusted): void
    {
        $this->untrusted = $untrusted;
    }

    /**
     * @return integer
     */
    public function getDeactivated()
    {
        return $this->deactivated;
    }

    /**
     * @param integer $deactivated
     */
    public function setDeactivated($deactivated): void
    {
        $this->deactivated = $deactivated;
    }

}