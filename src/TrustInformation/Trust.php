<?php

namespace RESWUE\Trust\TrustInformation;


class Trust
{
    private $decision;
    private $updatedAt;

    /**
     * Trust constructor.
     * @param array $data
     * @throws \Exception
     */
    public function __construct(array $data = [])
    {
        if (isset($data['decision'])) {
            $this->setDecision($data['decision']);
        }

        if (isset($data['updated_at'])) {
            $this->setUpdatedAt(new \DateTime($data['updated_at']));
        }
    }

    /**
     * @return string
     */
    public function getDecision()
    {
        return $this->decision;
    }

    /**
     * @param string $decision
     */
    public function setDecision($decision): void
    {
        $this->decision = $decision;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}